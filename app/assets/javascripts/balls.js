var colorValue = {
    "pink":   1,
    "green":  3,
    "blue":   5,
    "purple": 15
}

function scoreSum(color,currentScore){
    return colorValue[color.color] + parseInt(currentScore)
}
//generates ball span
function addBall(color, ballQuantity){
    var spanBall =  document.createElement("span");

    spanBall.classList.add('ball');
    spanBall.classList.add(color.color);
    spanBall.setAttribute('data-position', ballQuantity);

    return spanBall;
}
//get json with next ball color
function getNewBall(){
    var ballBox = document.querySelector("#balls");
    var ballQuantity = ballBox.querySelectorAll("span").length+1;
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.open('POST', '/balls/color', true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4) {
            if(xmlhttp.status == 200) {
                var color = JSON.parse(xmlhttp.responseText);

                var ballEl = addBall(color, ballQuantity);
                ballBox.appendChild(ballEl);
                //update number, could be better to implement the MutationObserver to fire
                //when new balls are created.
                var spanSum = document.querySelector("#sum");
                spanSum.textContent = ballQuantity;
                //update score, MutationObserver is a better implementation
                var score = document.querySelector("#score");
                score.textContent = scoreSum(color, score.innerText);
            }
        }
    };

    var currentBall = JSON.stringify({"color":{"currentNumber": ballQuantity}});
    xmlhttp.send(currentBall)
}

// tracks button click and fire getNewBall
var el = document.getElementById("drop-button");
el.addEventListener("click", getNewBall, false);

