module WhichColor
  BALL_COLORS = {0 => :pink, 3 => :green, 5 => :blue, 15 => :purple }
  BALL_COLORS.freeze

  def which_color(number)
    max_multiple = multiple(number).max
    BALL_COLORS.include?(max_multiple) ? BALL_COLORS[max_multiple] : :pink
  end


  private

  def multiple(number)
    multiples = [3, 5, 15]
    matched_multiples = []
    multiples.each { |multiple|
      matched_multiples << multiple if (number % multiple).zero?
    }
    matched_multiples
  end
end