class BallsController < ApplicationController
  include WhichColor
  skip_before_action :verify_authenticity_token

  def color
    next_color = which_color(params[:color][:currentNumber])
    respond_to do |format|
      format.json{ render json: { "color": next_color } }
    end
  end

  def home

  end
end
