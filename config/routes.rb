Rails.application.routes.draw do
  root 'balls#home'
  post 'balls/color'
  get 'static_pages/principal'
  get 'static_pages/sobre'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
